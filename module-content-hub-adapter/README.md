## Content Hub adapter for Greenfield Online

This directory contains example source plugins that can be used to
make content from one Greenfield Online installation available in
another Greenfield Online installation, using a Content Hub adapter.
There are two plugins, one for the installation that is to expose
content (`server-adapter`) and one for the installation that should
access adapted content (`client-adapter`).

This example adapts two content types, standard articles and images.


### Server Adapter plugin

The plugin containing the code for setting up an adapter web service
on top of a Greenfield Online installation is found in the
`server-adapter` module.
It consists of:

 * adapter-model-mapping.xml which sets up a greenfield variant using

 * changes-service-config.xml, which configures the changes service to
   include only major 1 and to use the input template filter.

 * input-template-filter-config.xml, which configures the input
   template filter to accept example.Image and example.StandardArticle.

The project must depend on this plugin in order to make content
accessible to a content hub in another system.


### Client Adapter plugin

The plugin for for adding an adapter to an remote content hub is found
in the `client-adapter` module. It consists of:

 * adapter_greenfield.xml, which configures content hub to use the
   adapter as major 1000, "Greenfield". The host and port are set
   using properties when building the plugin, defaulting to localhost
   on port 9090. (Build using -Dremote-content-hub-host=remote-host
   -Dremote-content-hub-port=remote-port to connect to a different
   server).

 * *-type.xml, model types which associates the input-templates to the
     corresponding beans.

 * *-template.xml, which is an input template for the wrapper content
   for the corresponding content type from the other system.

 * *-index-mapping.xml, which sets up index mappings for the wrapper
   contents.

 * permissions.xml, which makes the external contents from the other
   system readable to everyone.

The project must depend on this plugin in order to make content
from a content hub in another system available.
