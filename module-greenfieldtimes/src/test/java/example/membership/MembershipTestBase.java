package example.membership;

import com.google.gson.JsonElement;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.HttpMethod;
import org.mockito.Mock;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.octo.captcha.service.CaptchaServiceException;
import com.polopoly.cm.client.CMException;
import com.polopoly.paywall.PaywallProvider;
import com.polopoly.util.Base64;

import example.JettyWrapper;
import example.MockitoBase;
import example.captcha.ClusteredImageCaptchaService;
import example.membership.tools.SitePrefixUtil;

public class MembershipTestBase extends MockitoBase {

    protected static final String BUNDLE_ID_STR = "1.123";
    protected static final String ENCODED_BUNDLE_ID_STR = "MS4xMjM=";
    static final String TARGET_URI = "http://gt.com/mypage";
    static final String REFERRER = "http://gt.com/1.1293";

    static final String SERVLET_URI = "/membership";
    static final String SERVLET_URI_MATCHES = SERVLET_URI + "/*";

    static final String SITE_ID = "2.111";
    static final String LOGINNAME = "x@gt.com";
    static final String ENCODED_LOGINNAME = "eEBndC5jb20=";
    static final String PASSWORD = "123456";
    static final String SCREENNAME = "Mr X";
    static final String REAL_LOGINNAME = new SitePrefixUtil().addPrefix(SITE_ID, LOGINNAME);

    Gson gson = new Gson();
    JettyWrapper jetty;
    MembershipServletBase membershipServlet;

    @Mock
    ResetPasswordMailService resetPasswordMailService;

    @Mock
    PaywallProvider paywallProvider;

    public class DummyMembershipSettings implements MembershipSettings
    {
        public boolean isLoginAllowed(String siteIdString) throws CMException {
            return SITE_ID.equals(siteIdString);
        }

        public boolean isRegistrationAllowed(String siteIdString) throws CMException {
            return SITE_ID.equals(siteIdString);
        }

        public ResetPasswordMailService getResetPasswordMailService(
                String siteIdString) throws CMException {
            return resetPasswordMailService;
        }
    }

    protected void setUp()
        throws Exception
    {
        super.setUp();

        membershipServlet = new MembershipServletBase();
        membershipServlet.setMembershipSettings(new DummyMembershipSettings());
        membershipServlet.setPaywallProvider(paywallProvider);

        jetty = new JettyWrapper();
        jetty.addServlet(membershipServlet, SERVLET_URI_MATCHES);
    }

    void assertFormErrorsCookie(Cookie[] cookies,
                                JsonElement expectedErrors)
        throws Exception
    {
        assertCookie(cookies, expectedErrors, ActionRegister.ERROR_COOKIE_NAME);
    }

    void assertFormContentCookie(Cookie[] cookies,
                                 JsonElement expectedContent)
        throws Exception
    {
        assertCookie(cookies, expectedContent, ActionRegister.ECHO_COOKIE_NAME);
    }

    private void assertCookie(Cookie[] cookies,
                              JsonElement expectedJson,
                              String cookieName)
            throws IOException
    {
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(cookieName)) {
                String decodedCookie = new String(Base64.decode(cookie.getValue()));
                JsonElement cookieJson = new Gson().fromJson(decodedCookie, JsonElement.class);
                assertEquals("cookie " + cookieName + " incorrect value", expectedJson, cookieJson);
                return;
            }
        }

        fail("cookie " + cookieName + " did not exist");

    }

    void assertCacheHeadersNoCache(HttpMethod method)
    {
        assertEquals("private, no-store, no-cache, must-revalidate, max-age=0, s-max-age=0, post-check=0, pre-check=0",
                method.getResponseHeader("Cache-Control").getValue());
        assertEquals("no-cache", method.getResponseHeader("Pragma").getValue());
        assertEquals("Thu, 01 Jan 1970 00:00:00 GMT", method.getResponseHeader("Expires").getValue());
    }

    void assertCookie(Cookie[] cookies, String name, String value)
        throws IOException
    {
        assertCookie(cookies, new Gson().fromJson(value, JsonElement.class), name);
    }

    void assertDigestCookie(Cookie[] cookies, String name) {
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(name)) {
                assertEquals("Seems the digest values does not have correct length", 64, cookie.getValue().length());
            }
        }
    }

    protected void tearDown()
        throws Exception
    {
        super.tearDown();

        jetty.stop();
    }

    class DummyCaptchaService implements ClusteredImageCaptchaService {

        public BufferedImage getImageChallenge(HttpServletResponse respose)
                throws CaptchaServiceException {
            return null;
        }

        public boolean validateAnswer(String answer,
                HttpServletRequest request, HttpServletResponse respose)
                throws CaptchaServiceException {
            return true;
        }

        public boolean isEnabled()
        {
            return true;
        }

        public void setEnabled(boolean enabled)
        {
        }
    }
}
