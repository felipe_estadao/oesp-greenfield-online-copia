package example.filter;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mockito.Mock;

import com.polopoly.application.Application;
import com.polopoly.cm.ContentId;
import com.polopoly.cm.ExternalContentId;
import com.polopoly.cm.VersionedContentId;
import com.polopoly.cm.client.CmClient;
import com.polopoly.cm.client.ContentRead;
import com.polopoly.cm.path.ContentPathTranslator;
import com.polopoly.cm.policy.PolicyCMServer;
import com.polopoly.cm.servlet.RequestPreparator;

import example.MockitoBase;

public class OriginalImageFilterTest
    extends MockitoBase
{
    @Mock HttpServletRequest  request;
    @Mock HttpServletResponse response;

    @Mock FilterConfig config;
    @Mock FilterChain chain;

    @Mock Application app;
    @Mock CmClient cmClient;
    @Mock PolicyCMServer cm;

    @Mock ContentPathTranslator translator;

    @Mock ContentRead imageContent;
    @Mock ContentRead imageInputTemplateContent;

    @Mock ContentRead videoContent;
    @Mock ContentRead videoInputTemplateContent;

    OriginalImageFilter target;

    @Override
    protected void setUp()
        throws Exception
    {
        super.setUp();

        target = new OriginalImageFilter();

        when(config.getFilterName()).thenReturn("OriginalImageFilter");
        when(config.getInitParameter("inputTemplates")).thenReturn("example.Image,example.contenthub.Image");

        when(request.getAttribute(RequestPreparator.class.getName() + ".fcpt")).thenReturn(translator);

        when(request.getAttribute("p.application")).thenReturn(app);
        when(app.getPreferredApplicationComponent(CmClient.class)).thenReturn(cmClient);
        when(cmClient.getPolicyCMServer()).thenReturn(cm);

        when(translator.getContentId("/polopoly_fs/1.100")).thenReturn(new ContentId(1, 100));
        when(translator.getContentId("/polopoly_fs/1.100.1")).thenReturn(new ContentId(1, 100));
        when(translator.getContentId("/polopoly_fs/1.200")).thenReturn(new ContentId(1, 200));
        when(translator.getContentId("/polopoly_fs/myImage.extId")).thenReturn(new ContentId(1, 100));

        when(cm.getContent(new ContentId(1, 100))).thenReturn(imageContent);
        when(cm.getContent(new ContentId(1, 200))).thenReturn(videoContent);

        when(cm.getContent(new VersionedContentId(14, 1, 0))).thenReturn(imageInputTemplateContent);
        when(cm.getContent(new VersionedContentId(14, 3, 0))).thenReturn(videoInputTemplateContent);

        when(imageContent.getInputTemplateId()).thenReturn(new VersionedContentId(14, 1, 0));
        when(videoContent.getInputTemplateId()).thenReturn(new VersionedContentId(14, 3, 0));

        when(imageInputTemplateContent.getExternalId()).thenReturn(new ExternalContentId("example.Image"));
        when(videoInputTemplateContent.getExternalId()).thenReturn(new ExternalContentId("example.Video"));

        target.init(config);
    }

    public void test_accept_non_images()
        throws Exception
    {
        when(request.getPathInfo()).thenReturn("/polopoly_fs/1.200!file.mpg");

        target.doFilter(request, response, chain);

        verify(chain).doFilter(request, response);
    }

    public void test_refuse_original_image()
        throws Exception
    {
        when(request.getPathInfo()).thenReturn("/polopoly_fs/1.100!file.jpg");

        target.doFilter(request, response, chain);

        verify(chain, never()).doFilter(request, response);
        verify(response).sendError(HttpServletResponse.SC_NOT_FOUND);
    }

    public void test_refuse_original_image_with_path()
        throws Exception
    {
        when(request.getPathInfo()).thenReturn("/polopoly_fs/1.100!/image/file.jpg");

        target.doFilter(request, response, chain);

        verify(chain, never()).doFilter(request, response);
        verify(response).sendError(HttpServletResponse.SC_NOT_FOUND);
    }

    public void test_refuse_original_image_with_non_image_prefix()
        throws Exception
    {

      when(request.getPathInfo()).thenReturn("/polopoly_fs/1.100!file.mpg");

      target.doFilter(request, response, chain);

      verify(chain, never()).doFilter(request, response);
      verify(response).sendError(HttpServletResponse.SC_NOT_FOUND);
    }

    public void test_refuse_original_image_from_versioned_content()
        throws Exception
    {
        when(request.getPathInfo()).thenReturn("/polopoly_fs/1.100.1!file.jpg");

        target.doFilter(request, response, chain);

        verify(chain, never()).doFilter(request, response);
        verify(response).sendError(HttpServletResponse.SC_NOT_FOUND);
    }

    public void test_refuse_original_image_from_externalid_content()
        throws Exception
    {
        when(request.getPathInfo()).thenReturn("/polopoly_fs/myImage.extId!file.jpg");

        target.doFilter(request, response, chain);

        verify(chain, never()).doFilter(request, response);
        verify(response).sendError(HttpServletResponse.SC_NOT_FOUND);
    }

    public void test_allow_derivitive_image()
        throws Exception
    {
        when(request.getPathInfo()).thenReturn("/polopoly_fs/1.100!image/image.png_gen/derivatives/derivatives_300_400/image.png");

        target.doFilter(request, response, chain);

        verify(chain).doFilter(request, response);
    }
}
