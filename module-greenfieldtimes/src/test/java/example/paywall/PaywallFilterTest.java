package example.paywall;

import junit.framework.TestCase;

public class PaywallFilterTest extends TestCase {

    public void testExtractArticleIdString() throws Exception {
        assertEquals("1.172", PaywallFilter.extractArticleIdString("http://localhost:8080/cmlink/greenfield-times/luxury-car-classics-our-ten-favorites-1.172"));
        assertEquals("1.172", PaywallFilter.extractArticleIdString("http://localhost:8080/cmlink/greenfield-times/luxurycarclassicsourtenfavorites/1.172"));
        assertEquals("1.172", PaywallFilter.extractArticleIdString("1.172"));
    }
}
