package example.content.entity;

import com.polopoly.cm.app.imagemanager.ImageEditorPolicy;
import com.polopoly.model.Model;
import com.polopoly.model.ModelPathUtil;
import com.polopoly.render.CacheInfo;
import com.polopoly.render.RenderRequest;
import com.polopoly.siteengine.dispatcher.ControllerContext;
import com.polopoly.siteengine.model.TopModel;
import com.polopoly.siteengine.mvc.RenderControllerBase;

import example.layout.image.ImageFormatSetup;

public class RenderControllerPersonEntity
    extends RenderControllerBase
{
    public void populateModelAfterCacheKey(RenderRequest request,
                                           TopModel m,
                                           CacheInfo cacheInfo,
                                           ControllerContext context)
    {
        Model contentModel = context.getContentModel();

        try {
            PersonEntityPolicy person = (PersonEntityPolicy) ModelPathUtil.getBean(contentModel);
            ImageEditorPolicy imageEditor = person.getImageEditor();

            if (imageEditor != null && imageEditor.getReferredImageId() != null)
            {
                String derivateType = imageEditor.getSelectedDerivativeType();
                new ImageFormatSetup().setupImageDerivativeKeyInModel(m,
                                                                      derivateType,
                                                                      imageEditor.getImageSet());
                String position = derivateType == null || derivateType.equals("landscape") ? "top" : "inline";
                m.getLocal().setAttribute("position", position);
            }

        } catch (Exception e) {
            throw new RuntimeException("Unable to setup model", e);
        }

        super.populateModelAfterCacheKey(request, m, cacheInfo, context);

    }
}
