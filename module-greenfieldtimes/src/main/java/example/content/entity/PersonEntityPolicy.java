package example.content.entity;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.polopoly.cm.app.imagemanager.ImageEditorPolicy;
import com.polopoly.cm.app.imagemanager.ImageSet;
import com.polopoly.cm.client.CMException;
import com.polopoly.siteengine.standard.image.ImageResource;

public class PersonEntityPolicy
    extends EntityPolicy
    implements ImageResource
{
    private static final Logger LOG = Logger.getLogger(PersonEntityPolicy.class.getName());

    public ImageEditorPolicy getImageEditor()
        throws CMException
    {
        return (ImageEditorPolicy) getChildPolicy("image");
    }

    @Override
    public String getImageName()
    {
        try {
            return getImageEditor().getImageName();
        } catch (CMException e) {
            LOG.log(Level.WARNING, "Could not get image editor", e);
        }

        return null;
    }

    @Override
    public String getImageDescription()
    {
        try {
            return getImageEditor().getImageDescription();
        } catch (CMException e) {
            LOG.log(Level.WARNING, "Could not get image editor", e);
        }

        return null;
    }

    @Override
    public ImageSet getImageSet()
    {
        try {
            return getImageEditor().getImageSet();
        } catch (CMException e) {
            LOG.log(Level.WARNING, "Could not get image editor", e);
        }

        return null;
    }
}
