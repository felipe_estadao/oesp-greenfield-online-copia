package example.content.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.lang.StringUtils;

import com.polopoly.cm.app.policy.SingleValuePolicy;
import com.polopoly.cm.client.CMException;
import com.polopoly.metadata.Attribute;
import com.polopoly.metadata.Entity;
import com.polopoly.metadata.EntityContainer;

public class OrganizationEntityPolicy extends EntityPolicy {

    private static final String SUMMARY = "summary";

    public String getSummary() {
        String summary = null;
        try {
            summary = ((SingleValuePolicy) getChildPolicy(SUMMARY)).getValue();
        } catch (CMException e) {
            logger.log(Level.SEVERE, "Couldn't read child policy with name '" + SUMMARY + "'", e);
        }
        return summary;
    }

    @Override
    public EntityContainer getEntityWithChildren(int maxDepth) {
        Entity entity = (Entity) super.getEntityWithChildren(maxDepth);
        return populateEntityWithAttributes(entity);
    }

    private Entity populateEntityWithAttributes(Entity toEnrich) {
        List<Attribute> attributes = new ArrayList<Attribute>();
        String summary = getSummary();
        if (summary != null) {
            attributes.add(new Attribute(StringUtils.capitalize(SUMMARY), summary));
        }
        toEnrich.setAttributes(attributes );
        return toEnrich;
    }
}
