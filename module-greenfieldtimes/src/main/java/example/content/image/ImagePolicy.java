package example.content.image;

import com.atex.onecms.content.ContentResult;
import com.atex.onecms.content.ContentWrite;
import com.atex.onecms.content.LegacyContentAdapter;
import com.atex.onecms.content.metadata.MetadataInfo;
import com.atex.onecms.ws.service.AuthenticationUtil;
import com.polopoly.cm.ContentFileInfo;
import com.polopoly.cm.app.Resource;
import com.polopoly.cm.app.imagemanager.ImageFormatException;
import com.polopoly.cm.app.imagemanager.ImageManagerPolicy;
import com.polopoly.cm.app.imagemanager.ImageSet;
import com.polopoly.cm.app.imagemanager.ImageTooBigException;
import com.polopoly.cm.app.policy.SingleValued;
import com.polopoly.cm.client.CMException;
import com.polopoly.cm.client.ContentRead;
import com.polopoly.cm.policymvc.PolicyModelDomain;
import com.polopoly.model.Model;
import com.polopoly.model.ModelPathUtil;
import com.polopoly.model.pojo.FileReference;
import com.polopoly.user.server.Caller;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import example.content.ImageProviderPolicy;
import example.greenfieldtimes.adapter.ImageResourceBean;

import javax.ws.rs.core.MediaType;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Policy representing an image resource.
 */

public class ImagePolicy extends ImageProviderPolicy
    implements Resource, LegacyContentAdapter<ImageResourceBean>
{

    protected static final String BINARY_IMAGE = "image";
    Logger LOG = Logger.getLogger(ImageProviderPolicy.class.getName());

    /**
     * Get image specific clip
     */
    public Map<String, String> getResourceData() throws CMException
    {
        try {
            ImageSet selectedImage = getImageSet();

            if (selectedImage != null) {
                Map<String, String> map = new HashMap<String, String>();

                // Use original image when pasting copy/pasting into an p.ImageManager.
                // Note that getFileInfo and exportFile must support this path,
                // i.e. derivates are not supported unless front generation is disabled
                // and exclusively using p.ImageManager (not p.HttpImageManager).
                String originalPath = selectedImage.getImage().getPath();
                // Use landscape_490 derivative when copy/pasting images
                String path = selectedImage.getImage("landscape_490").getPath();
                map.put(Resource.FIELD_CONTENT_FILE_PATH, path);
                map.put(Resource.FIELD_RESOURCE_TYPE, BINARY_IMAGE);
                map.put(Resource.FIELD_IMG_ALT, getName());
                map.put(Resource.FIELD_IMAGE_CONTENT_FILE_PATH, originalPath);

                if (selectedImage.getImage().isAbsolute()) {
                    map.put(Resource.FIELD_AUTHORITATIVE_FILE_URL, path);
                }

                return map;
            }
        } catch (Exception e) {
            logger.logp(Level.WARNING, CLASS, "getResourceData",
                    "Failed to create resource data for " + getContentId(), e);
        }

        return null;
    }

    @Override
    public ContentFileInfo getFileInfo(String path)
        throws CMException, IOException
    {
        try {
            ImageSet imageSet = getImageSet();
            if (imageSet instanceof ContentRead) {
                return ((ContentRead) imageSet).getFileInfo(path);
            }

        } catch (FileNotFoundException ignore) { }

        return super.getFileInfo(path);
    }

    @Override
    public void exportFile(String path, OutputStream data)
        throws CMException, IOException
    {
        try {
            ImageSet imageSet = getImageSet();
            if (imageSet instanceof ContentRead) {
                ((ContentRead) imageSet).exportFile(path, data);
                return;
            }

        } catch (FileNotFoundException ignore) { }

        super.exportFile(path, data);
    }


    @Override
    public ContentResult<ImageResourceBean> legacyToNew(PolicyModelDomain policyModelDomain)
        throws CMException
    {
        // Use model to simplify dealing with child policies
        Model imageModel = policyModelDomain.getModel(getContentId());

        ImageResourceBean imageResourceBean = new ImageResourceBean();
        imageResourceBean.setName(getName());
        imageResourceBean.setDescription(ModelPathUtil.get(imageModel, "description/value", String.class));

        String imagePath = getImagePath();
        if (imagePath != null) {
            imageResourceBean.setFileReference(new FileReference(imagePath));
        }

        try {
            if (getMediumThumbnail() != null) {
                imageResourceBean.setThumbnailUrl(getMediumThumbnail().getPath());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        imageResourceBean.setByline(ModelPathUtil.get(imageModel, "byline/value", String.class));
        imageResourceBean.setContact(ModelPathUtil.get(imageModel, "contact/value", String.class));
        imageResourceBean.setLicenseUrl(ModelPathUtil.get(imageModel, "licenseurl/value", String.class));

        return new ContentResult<>(imageResourceBean);
    }

    @Override
    public void newToLegacy(ContentWrite<ImageResourceBean> contentWrite)
        throws CMException
    {
        MetadataInfo metadata = (MetadataInfo) contentWrite.getAspect("metadata");
        if (metadata != null) {
            setMetadata(metadata.getMetadata());
        }

        ImageResourceBean bean = contentWrite.getContentData();

        if (bean == null) {
            return;
        }

        FileReference fileReference = bean.getFileReference();

        try {
            updateImageData(fileReference, getImagePath());
        } catch (Exception e) {
            throw new CMException(e);
        }

        this.setName(bean.getName());
        ((SingleValued) getChildPolicy("description")).setValue(bean.getDescription());
        ((SingleValued) getChildPolicy("byline")).setValue(bean.getByline());
        ((SingleValued) getChildPolicy("contact")).setValue(bean.getContact());
        ((SingleValued) getChildPolicy("licenseurl")).setValue(bean.getLicenseUrl());
    }

    private String getImagePath() throws CMException
    {
        ImageSet selectedImage = getImageSet();

        if (selectedImage != null) {
            String imagePath = null;
            try {
                return selectedImage.getImage().getPath();
            } catch (IOException e) {
                String logMessage = String.format("Unable to get image in content using path: '%s'.", imagePath);
                LOG.log(Level.WARNING, logMessage, e);
            }
        }

        return null;
    }

    private void updateImageData(FileReference image, String currentPath)
        throws CMException, IOException, ImageFormatException, ImageTooBigException
    {
        ImageManagerPolicy imageManager = getImageProvider();

        if (currentPath == null) {
            if (noFile(image)) {
                // Nothing selected and nothing should be selected
                return;
            }
            // Import new image
            FileStream data = getStreamForPath(image.getUrl(), imageManager.getCMServer().getCurrentCaller());
            try {
                String importImage = imageManager.importImage(data.fileName, data.stream);
                imageManager.setSelectedImage(importImage);
            } finally {
                data.stream.close();
            }
            return;
        }

        if (image != null && currentPath.equals(image.getFilePath())
            && image.getUrl() == null)
        {
            // No new image data.
            return;
        }

        if (noFile(image)) {
            // Remove old image
            imageManager.setSelectedImage(null);
            return;
        }

        // Overwrite old image
        FileStream data = getStreamForPath(image.getUrl(), imageManager.getCMServer().getCurrentCaller());
        try {
            String importImage = imageManager.importImage(data.fileName, data.stream);
            imageManager.setSelectedImage(importImage);
        } finally {
            data.stream.close();
        }
    }

    private boolean noFile(FileReference image) {
        if (image == null) {
            return true;
        }
        if (image.getUrl() == null) {
            return true;
        }
        return image.getUrl().length() == 0;
    }

    private static class FileStream {
        public final String fileName;
        public final InputStream stream;
        public FileStream(String fileName, InputStream stream) {
            this.fileName = fileName;
            this.stream = stream;
        }
    }

    private FileStream getStreamForPath(String filePath, Caller caller)
        throws CMException
    {
        WebResource resource = Client.create().resource(filePath);
        String token = AuthenticationUtil.getAuthToken(caller);
        ClientResponse resp;
        try {
            resp = resource.header("X-Auth-Token", token).get(ClientResponse.class);
        } catch (ClientHandlerException e) {
            if (e.getCause() instanceof ConnectException) {
                throw new CMException(e.getCause().getMessage(), e.getCause());
            } else {
                throw e;
            }
        }
        String type = resp.getHeaders().getFirst("Content-Type");
        String path;
        if (type == null) {
            path = "image.jpg";
        } else {
            MediaType mt = MediaType.valueOf(type);
            if ("jpeg".equals(mt.getSubtype())) {
                path = "image.jpg";
            } else if (!"*".equals(mt.getSubtype())) {
                path = "image." + mt.getSubtype();
            } else {
                path = "image.jpg";
            }
        }

        return new FileStream(path, resp.getEntityInputStream());
    }
}
