package example.content;

import com.polopoly.metadata.Metadata;
import com.polopoly.metadata.MetadataAware;
import com.polopoly.metadata.util.MetadataUtil;

/**
 * Base policy for resources.
 *
 */
public abstract class ResourceBasePolicy extends ContentBasePolicy
    implements ResourceContent
{
    private static final String[] EMPTY_STRING_ARRAY = new String[0];

    public String getByline() {
        return getChildValue("byline", null);
    }

    public String[] getKeywords() {
        String keywords = getChildValue("keywords");
        return (keywords != null && keywords.length() > 0) ? keywords.split("[ |,] *") : EMPTY_STRING_ARRAY;
    }

    private MetadataAware getMetadataAware()
    {
        return MetadataUtil.getMetadataAware(this);
    }

    @Override
    public Metadata getMetadata() {
        return getMetadataAware().getMetadata();
    }

    @Override
    public void setMetadata(Metadata arg0) {
        getMetadataAware().setMetadata(arg0);
    }
}
