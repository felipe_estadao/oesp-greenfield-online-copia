package example.content.file;

import com.atex.onecms.content.ContentResult;
import com.atex.onecms.content.ContentWrite;
import com.atex.onecms.content.LegacyContentAdapter;
import com.atex.onecms.content.metadata.MetadataInfo;
import com.atex.onecms.ws.service.AuthenticationUtil;
import com.atex.plugins.baseline.url.UrlResolver;
import com.polopoly.cm.ContentId;
import com.polopoly.cm.ExternalContentId;
import com.polopoly.cm.app.Resource;
import com.polopoly.cm.app.policy.FilePolicy;
import com.polopoly.cm.app.policy.SingleValued;
import com.polopoly.cm.client.CMException;
import com.polopoly.cm.client.Content;
import com.polopoly.cm.policymvc.PolicyModelDomain;
import com.polopoly.model.Model;
import com.polopoly.model.ModelPathUtil;
import com.polopoly.model.pojo.FileReference;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import example.content.ResourceBasePolicy;
import example.greenfieldtimes.adapter.FileBean;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A Policy representing a file resource.
 */
public class FileResourcePolicy extends ResourceBasePolicy
    implements Resource, LegacyContentAdapter<FileBean>
{
    private static final Logger LOG = Logger.getLogger(FileResourcePolicy.class.getName());
    private static final ContentId ICON_EXTERNAL_ID = new ExternalContentId("p.Icons");

    private static final String RESOURCE_TYPE = "file";

    private ContentId iconContentId;
    private FileExistsChecker fileExistsChecker;

    protected void initSelf()
    {
        super.initSelf();

        try {
            Content iconContent = (Content) getCMServer().getContent(ICON_EXTERNAL_ID);

            iconContentId = iconContent.getContentId();
            fileExistsChecker = new FileExistsChecker(iconContent);
        } catch (CMException e) {
            LOG.log(Level.WARNING, "Unable to find icon content", e);
            iconContentId = ICON_EXTERNAL_ID;
        }
    }

    /**
     * Returns the icon path for the image size "48x48".
     *
     * @param urlResolver the URL resolver to use
     * @return the icon path
     */
    public String getThumbnailPath(UrlResolver urlResolver)
    {
        return getIconPath(urlResolver, "48x48");
    }

    /**
     * Returns the icon path for the image size "16x16".
     *
     * @param urlResolver the URL resolver to use
     * @return the icon path
     */
    public String getSmallIconPath(UrlResolver urlResolver)
    {
        return getIconPath(urlResolver, "16x16");
    }

    /**
     * Returns the path to the icon for a specified image size.
     * The image size can either be "16x16" or "48x48".
     *
     * @param urlResolver the URL resolver to use
     * @param imageSize the image size
     * @return the icon path
     */
    String getIconPath(UrlResolver urlResolver,
                       String imageSize)
    {
        String iconPath = null;
        String mimeType = null;

        try {
            mimeType = getFilePolicy().getMimeType();

            String fileName = "image/" + imageSize + "/" + mimeType + ".png";

            if (fileExistsChecker == null || !fileExistsChecker.fileExists(fileName)) {
                fileName = "/image/" + imageSize +
                           "/application/octet-stream.png";
            }

            iconPath = urlResolver.getFileUrl(iconContentId, fileName);
        } catch (Exception e) {
            LOG.log(Level.WARNING, "Unable to get icon file info for mime-type: " + mimeType, e);
        }

        return iconPath;
    }

    /**
     * Returns the full path to the file
     *
     * @param urlResolver the URL resolver to use
     */
    public String getPreviewPath(UrlResolver urlResolver)
    {
        String previewPath = null;

        try {
            String path = getFilePolicy().getFullFilePath();

            if (path != null) {
                previewPath = urlResolver.getFileUrl(getContentId(), path);
            }
        } catch (CMException e) {
            LOG.log(Level.WARNING, "Unable to resolve file preview path", e);
        }

        return previewPath;
    }

    /**
     * Returns the file policy
     *
     * @return the file policy of this file resource
     * @throws CMException if something goes wrong
     */
    public FilePolicy getFilePolicy()
        throws CMException
    {
        return (FilePolicy) getChildPolicy("file");
    }

    public Map<String, String> getResourceData()
        throws CMException
    {
        try {
            FilePolicy filePolicy = getFilePolicy();
            String fileName = filePolicy.getFileName();

            if (fileName != null) {
                Map<String, String> map = new HashMap<String, String>();

                String filePath = filePolicy.getFullFilePath();

                map.put(Resource.FIELD_CONTENT_FILE_PATH, filePath);
                map.put(Resource.FIELD_RESOURCE_TYPE, RESOURCE_TYPE);
                map.put(Resource.FIELD_IMG_ALT, getName());

                return map;
            }
        } catch (Exception e) {
            logger.logp(Level.WARNING, CLASS, "getResourceData",
                    "Failed to create resource data for " + getContentId(), e);
        }

        return null;
    }

    @Override
    public ContentResult<FileBean> legacyToNew(final PolicyModelDomain policyModelDomain) throws CMException
    {
        FileBean fileBean = new FileBean();
        Model model = policyModelDomain.getModel(this);

        fileBean.setName(getName());
        fileBean.setDescription(ModelPathUtil.get(model, "description/value", String.class));

        String filePath = getFilePolicy().getFullFilePath();
        if (filePath != null) {
            FileReference fileReference = new FileReference(filePath);
            try {
                String mimeType = getFilePolicy().getFileInfo(filePath).getMimeType();
                fileReference.setMimeType(mimeType);
                fileBean.setFileReference(fileReference);
            } catch (IOException e) {
                throw new CMException(e);
            }
        }

        fileBean.setByline(ModelPathUtil.get(model, "byline/value", String.class));
        fileBean.setContact(ModelPathUtil.get(model, "contact/value", String.class));
        fileBean.setLicenseUrl(ModelPathUtil.get(model, "licenseurl/value", String.class));

        return new ContentResult<>(fileBean, new HashMap<String, Object>());
    }

    @Override
    public void newToLegacy(final ContentWrite<FileBean> contentWrite) throws CMException
    {
        MetadataInfo metadata = (MetadataInfo) contentWrite.getAspect("metadata");
        if (metadata != null) {
            setMetadata(metadata.getMetadata());
        }

        FileBean bean = contentWrite.getContentData();

        if (bean == null) {
            return;
        }

        FileReference fileReference = bean.getFileReference();

        if (fileReference.getFilePath() == null) {
            fileReference.setFilePath(bean.getName());
        }

        storeFileInContent(bean);
        populateModelFromBean(bean);
    }
    private void populateModelFromBean(FileBean fileBean)
        throws CMException
    {
        this.setName(fileBean.getName());
        ((SingleValued) getChildPolicy("lead")).setValue(fileBean.getDescription());
        ((SingleValued) getChildPolicy("byline")).setValue(fileBean.getByline());
        ((SingleValued) getChildPolicy("contact")).setValue(fileBean.getContact());
        ((SingleValued) getChildPolicy("licenseurl")).setValue(fileBean.getLicenseUrl());
    }

    private void storeFileInContent(FileBean fileBean)
        throws CMException
    {
        FilePolicy filePolicy = this.getFilePolicy();
        FileReference fileReference = fileBean.getFileReference();
        InputStream data = getInputStreamFromUrl(fileBean);
        if (fileReference.getUrl() != null) {
            filePolicy.setFullFilePath(fileReference.getFilePath());
            try {
                int lastSlashIndex = fileReference.getFilePath().lastIndexOf("/");
                if (lastSlashIndex >= 0) {
                    String directoryPath = fileReference.getFilePath().substring(0, lastSlashIndex);
                    try {
                        filePolicy.getFileInfo(directoryPath);
                    } catch (FileNotFoundException createDiretories) {
                        LOG.log(Level.FINEST, "Creating directory: " + directoryPath);
                        filePolicy.createDirectory(directoryPath, true);
                    }
                }
                filePolicy.importFile(fileReference.getFilePath(), data);
            } catch (IOException e) {
                LOG.log(Level.WARNING, "Unable to import file: '" + fileReference.getUrl()
                    + "' in content: '" + this.getContentId() + "'.",
                    e);
                throw new CMException(e.getMessage(), e);
            } finally {
                if (data != null) {
                    try {
                        data.close();
                    } catch (IOException e) {
                        LOG.log(Level.WARNING, "Unable to close stream for: '" + fileReference.getUrl()
                            + "' in content: '" + this.getContentId() + "'.",
                            e);
                    }
                }
            }
        }
    }

    private InputStream getInputStreamFromUrl(FileBean fileBean)
        throws CMException
    {
        FileReference file = fileBean.getFileReference();
        WebResource resource = Client.create().resource(file.getUrl());
        String token = AuthenticationUtil.getAuthToken(getCMServer().getCurrentCaller());
        ClientResponse resp;
        try {
            resp = resource.header("X-Auth-Token", token).get(ClientResponse.class);
        } catch (ClientHandlerException e) {
            if (e.getCause() instanceof ConnectException) {
                throw new CMException(e.getCause().getMessage(), e.getCause());
            } else {
                throw e;
            }
        }
        return resp.getEntityInputStream();
    }
}
