package example.content.link;

import com.atex.onecms.content.ContentResult;
import com.atex.onecms.content.ContentWrite;
import com.atex.onecms.content.LegacyContentAdapter;
import com.atex.onecms.content.metadata.MetadataInfo;
import com.atex.plugins.baseline.url.UrlResolver;
import com.polopoly.cm.app.policy.ContentReferencePolicy;
import com.polopoly.cm.app.policy.SelectableSubFieldPolicy;
import com.polopoly.cm.app.policy.SingleValuePolicy;
import com.polopoly.cm.client.CMException;
import com.polopoly.cm.policymvc.PolicyModelDomain;
import com.polopoly.model.ModelPathUtil;
import com.polopoly.model.pojo.FileReference;
import example.content.ContentBasePolicy;
import example.content.ResourceBasePolicy;
import example.greenfieldtimes.adapter.LinkBean;

/**
 * Policy representing a link resource.
 */
public class LinkResourcePolicy
    extends ResourceBasePolicy
    implements LegacyContentAdapter<LinkBean>
{

    public String getName()
        throws CMException
    {
        // As default, use the name of the link content itself

        String contentName = super.getName();

        if (null != contentName && contentName.trim().length() > 0) {
            return contentName;
        }

        // Fallback on the external URL or the name of the linked content

        SelectableSubFieldPolicy subFieldPolicy =
            (SelectableSubFieldPolicy) getChildPolicy("link");

        if (null != subFieldPolicy) {
            String selected = subFieldPolicy.getSelectedSubFieldName();

            if (null != selected) {
                ContentBasePolicy selectedFieldPolicy = (ContentBasePolicy) subFieldPolicy.getChildPolicy(selected);

                if ("internal".equals(selected)) {
                  ContentReferencePolicy contentRef =
                      (ContentReferencePolicy) selectedFieldPolicy.getChildPolicy("content");

                  return getInternalLinkName(contentRef);
                } else if ("external".equals(selected)) {
                    SingleValuePolicy urlPolicy =
                        (SingleValuePolicy) selectedFieldPolicy.getChildPolicy("href");

                    return getExternalLinkName(urlPolicy);
                }
            }
        }

        return "";
    }

    private String getExternalLinkName(SingleValuePolicy urlLinkPolicy)
        throws CMException {

        if (null != urlLinkPolicy) {
            String url = urlLinkPolicy.getValue();

            if (url.startsWith("http://")) {
                url = url.substring(7);
            }

            return url;
        }

        return "";
    }

    private String getInternalLinkName(ContentReferencePolicy contentSelectPolicy)
        throws CMException {

        if (null != contentSelectPolicy) {
            return getCMServer().getContent(
                    contentSelectPolicy.getReference()).getName();
        }

        return "";
    }

    public String getThumbnailPath(UrlResolver urlResolver)
    {
        return null;
    }

    public String getPreviewPath(UrlResolver urlResolver)
    {
        return null;
    }

    public String getPathSegmentString() throws CMException {
        return null;
    }

    @Override
    public ContentResult<LinkBean> legacyToNew(final PolicyModelDomain policyModelDomain) throws CMException
    {
        LinkBean bean = new LinkBean();

        bean.setName(getName());

        FileReference fileReference = new FileReference();
        fileReference.setMimeType("text/x-url");
        fileReference.setUrl((String) ModelPathUtil.get(policyModelDomain.getModel(this), "link/selected/href/value"));
        bean.setFileReference(fileReference);

        return new ContentResult<>(bean);
    }

    @Override
    public void newToLegacy(final ContentWrite<LinkBean> contentWrite) throws CMException
    {
        MetadataInfo metadata = (MetadataInfo) contentWrite.getAspect("metadata");
        if (metadata != null) {
            setMetadata(metadata.getMetadata());
        }

        LinkBean bean = contentWrite.getContentData();

        if (bean == null) {
            return;
        }

        this.setName(bean.getName());
        this.setComponent("link", "subField", "external");
        this.setComponent("link/external/href", "value", bean.getFileReference().getUrl());
    }
}
