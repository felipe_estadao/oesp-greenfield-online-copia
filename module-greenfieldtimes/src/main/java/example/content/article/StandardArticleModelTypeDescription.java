package example.content.article;

import java.util.List;

import com.polopoly.cm.client.CMException;

public interface StandardArticleModelTypeDescription
{
    String getAuthor() throws CMException;

    List<String> getAssociatedUsers() throws CMException;

    long getPublishingDateTime();

    String getPrimaryLocation() throws CMException;

    String getPrimaryTag() throws CMException;
}
