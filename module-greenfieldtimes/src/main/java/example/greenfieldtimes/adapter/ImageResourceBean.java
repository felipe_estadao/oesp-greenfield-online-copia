package example.greenfieldtimes.adapter;

import com.atex.onecms.content.resource.ResourceBean;

public class ImageResourceBean extends ResourceBean
{
    private String description;
    private String byline;
    private String contact;
    private String licenseUrl;

    public ImageResourceBean()
    {

    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getByline()
    {
        return byline;
    }

    public void setByline(String byline)
    {
        this.byline = byline;
    }

    public String getContact()
    {
        return contact;
    }

    public void setContact(String contact)
    {
        this.contact = contact;
    }

    public String getLicenseUrl()
    {
        return licenseUrl;
    }

    public void setLicenseUrl(String licenseUrl)
    {
        this.licenseUrl = licenseUrl;
    }
}
