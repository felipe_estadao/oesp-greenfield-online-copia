package example.greenfieldtimes.adapter;

import com.polopoly.cm.ContentId;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class ArticleBean
{
    private String title;
    private String lead;
    private String body;
    private String byline;
    private String editorialNotes;
    private Date createdDate;
    private Date modifiedDate;
    private Date publishedDate;
    private Map<String, String> customAttributes;
    private String priority;

    // Content lists contain different types
    private List<ContentId> topImages;
    private List<ContentId> images;
    private List<ContentId> relatedArticles;
    private List<ContentId> resources;

    public ArticleBean()
    {

    }

    /**
     * Copy constructor.
     *
     * @param orig bean to copy
     */
    public ArticleBean(ArticleBean orig)
    {
        this.body = orig.getBody();
        this.byline = orig.getByline();
        this.lead = orig.getLead();
        this.title = orig.getTitle();
        this.editorialNotes = orig.getEditorialNotes();
        this.createdDate = orig.getCreatedDate();
        this.modifiedDate = orig.getModifiedDate();
        this.publishedDate = orig.getPublishedDate();
        this.customAttributes = orig.getCustomAttributes();
        this.priority = orig.getPriority();
        this.topImages = orig.getTopImages();
        this.images = orig.getImages();
        this.relatedArticles = orig.getRelatedArticles();
        this.resources = orig.getResources();
    }


    public String getTitle()
    {
        return title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }
    public String getLead()
    {
        return lead;
    }
    public void setLead(String lead)
    {
        this.lead = lead;
    }
    public String getBody()
    {
        return body;
    }
    public void setBody(String body)
    {
        this.body = body;
    }
    public String getByline()
    {
        return byline;
    }
    public void setByline(String byline)
    {
        this.byline = byline;
    }
    public String getEditorialNotes()
    {
        return editorialNotes;
    }
    public void setEditorialNotes(String editorialNotes)
    {
        this.editorialNotes = editorialNotes;
    }
    public Date getCreatedDate()
    {
        return createdDate;
    }
    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }
    public Date getModifiedDate()
    {
        return modifiedDate;
    }
    public void setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }
    public Date getPublishedDate()
    {
        return publishedDate;
    }
    public void setPublishedDate(Date publishedDate)
    {
        this.publishedDate = publishedDate;
    }
    public Map<String, String> getCustomAttributes()
    {
        return customAttributes;
    }
    public void setCustomAttributes(Map<String, String> customAttributes)
    {
        this.customAttributes = customAttributes;
    }
    public String getPriority()
    {
        return priority;
    }
    public void setPriority(String priority)
    {
        this.priority = priority;
    }
    public List<ContentId> getTopImages()
    {
        return topImages;
    }
    public void setTopImages(List<ContentId> topImages)
    {
        this.topImages = topImages;
    }
    public List<ContentId> getImages()
    {
        return images;
    }
    public void setImages(List<ContentId> images)
    {
        this.images = images;
    }
    public List<ContentId> getRelatedArticles()
    {
        return relatedArticles;
    }
    public void setRelatedArticles(List<ContentId> relatedArticles)
    {
        this.relatedArticles = relatedArticles;
    }
    public List<ContentId> getResources()
    {
        return resources;
    }
    public void setResources(final List<ContentId> resources)
    {
        this.resources = resources;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (!(o instanceof ArticleBean))
        {
            return false;
        }

        ArticleBean that = (ArticleBean) o;

        if (body != null ? !body.equals(that.body) : that.body != null)
        {
            return false;
        }
        if (byline != null ? !byline.equals(that.byline) : that.byline != null)
        {
            return false;
        }
        if (createdDate != null ? !createdDate.equals(that.createdDate) : that.createdDate != null)
        {
            return false;
        }
        if (customAttributes != null ? !customAttributes
                                            .equals(that.customAttributes) : that.customAttributes != null)
        {
            return false;
        }
        if (editorialNotes != null ? !editorialNotes
                                          .equals(that.editorialNotes) : that.editorialNotes != null)
        {
            return false;
        }
        if (images != null ? !images.equals(that.images) : that.images != null)
        {
            return false;
        }
        if (lead != null ? !lead.equals(that.lead) : that.lead != null)
        {
            return false;
        }
        if (modifiedDate != null ? !modifiedDate
                                        .equals(that.modifiedDate) : that.modifiedDate != null)
        {
            return false;
        }
        if (priority != null ? !priority.equals(that.priority) : that.priority != null)
        {
            return false;
        }
        if (publishedDate != null ? !publishedDate
                                         .equals(that.publishedDate) : that.publishedDate != null)
        {
            return false;
        }
        if (relatedArticles != null ? !relatedArticles
                                           .equals(that.relatedArticles) : that.relatedArticles != null)
        {
            return false;
        }
        if (resources != null ? !resources.equals(that.resources) : that.resources != null)
        {
            return false;
        }
        if (title != null ? !title.equals(that.title) : that.title != null)
        {
            return false;
        }
        if (topImages != null ? !topImages.equals(that.topImages) : that.topImages != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (lead != null ? lead.hashCode() : 0);
        result = 31 * result + (body != null ? body.hashCode() : 0);
        result = 31 * result + (byline != null ? byline.hashCode() : 0);
        result = 31 * result + (editorialNotes != null ? editorialNotes.hashCode() : 0);
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        result = 31 * result + (modifiedDate != null ? modifiedDate.hashCode() : 0);
        result = 31 * result + (publishedDate != null ? publishedDate.hashCode() : 0);
        result = 31 * result + (customAttributes != null ? customAttributes.hashCode() : 0);
        result = 31 * result + (priority != null ? priority.hashCode() : 0);
        result = 31 * result + (topImages != null ? topImages.hashCode() : 0);
        result = 31 * result + (images != null ? images.hashCode() : 0);
        result = 31 * result + (relatedArticles != null ? relatedArticles.hashCode() : 0);
        result = 31 * result + (resources != null ? resources.hashCode() : 0);
        return result;
    }
}
