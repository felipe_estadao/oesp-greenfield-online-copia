package example.data;

import com.polopoly.cm.ContentId;
import com.polopoly.content.integration.AdapterContentId;
import com.atex.onecms.content.ContentResult;
import com.atex.onecms.content.mapping.ContentComposer;
import com.atex.onecms.content.mapping.Context;
import com.atex.onecms.content.mapping.Request;
import example.greenfieldtimes.adapter.ArticleBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Mapper between {@link ArticleBean}:s model type and the example.StandardArticle model type
 * (or anything with the same model paths, such as example.contenthub.StandardArticle).
 */
public class ContentHubArticleComposer
    implements ContentComposer<ArticleBean, ArticleBean, Object>
{

    private List<AdapterContentId> getAdapterContentIdList(List<ContentId> ids)
    {
        if (ids == null) {
            return Collections.emptyList();
        }
        @SuppressWarnings("unchecked")
        List<AdapterContentId> adaptedList = new ArrayList<AdapterContentId>(ids.size());
        for (ContentId id : ids)
        {
            adaptedList.add(new AdapterContentId(id.getContentIdString()));
        }

        return adaptedList;
    }

    @Override
    public ContentResult<ArticleBean> compose(ContentResult<ArticleBean> articleBeanDataResult, String s,
                                       Request request, Context<Object> context)
    {
        ArticleBean original = articleBeanDataResult.getContent().getContentData();
        ArticleBean articleBean = new ArticleBean(original);
        articleBean.setTopImages((List) getAdapterContentIdList(original.getTopImages()));
        articleBean.setImages((List) getAdapterContentIdList(original.getImages()));
        articleBean.setRelatedArticles((List) getAdapterContentIdList(original.getRelatedArticles()));

        return new ContentResult<ArticleBean>(articleBeanDataResult, articleBean);
    }
}
