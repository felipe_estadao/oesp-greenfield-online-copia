package example.data;

import com.atex.onecms.content.ContentResult;
import com.atex.onecms.content.ContentVersionId;
import com.atex.onecms.content.Status;
import com.atex.onecms.content.Subject;
import com.atex.onecms.ws.service.ErrorResponse;
import com.atex.onecms.ws.service.ErrorResponseException;
import com.atex.onecms.ws.service.WebServiceUtil;
import com.sun.jersey.spi.resource.PerRequest;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * A simple JAX-RS implementation of a custom content service. Works a lot like the standard content
 * service, but instead of checking CM permissions it checks only that the content belongs to the
 * active site. This is intended to be used for the same kind of content that would be available
 * using Site Engine, i.e. if it has a controller (or output template, in Site Engine) and is made
 * public, it is available on the web.
 */
@Path("content")
@PerRequest
public class CustomContentService
{
    @Context Request request;

    /**
     * WebserviceUtil needs to be set up per request, which is why this class must be annotated with @PerRequest.
     */
    WebServiceUtil webServiceUtil;

    /**
     * Creates an instance with a WebServiceUtil. Will be called by the JAX-RS implementation
     * (e.g. Jersey).
     */
    public CustomContentService(@Context ServletContext servletContext,
                                @Context ServletConfig servletConfig,
                                @Context HttpHeaders httpHeaders,
                                @Context UriInfo uriInfo)
    {
        webServiceUtil = new WebServiceUtil(servletContext, servletConfig, httpHeaders, uriInfo);
    }

    @GET
    @Path("contentid/{id}")
    @Produces({"application/xml; charset=utf-8", "application/json; charset=utf-8"})
    public Response getContentWithContentId(@PathParam("id") String id)
        throws ErrorResponseException
    {
        if(!webServiceUtil.isVersionedIdString(id)) {
            return webServiceUtil.resolveAndForward(webServiceUtil.parseId(id));
        }

        ContentVersionId vId = webServiceUtil.parseVersionedId(id);

        if (!checkPermission(vId)) {
            throw webServiceUtil.errorForbidden("Permission denied.");
        }

        ContentResult<?> data = webServiceUtil.get(vId, webServiceUtil.getVariant());
        Status status = data.getStatus();
        if (status.isOk()) {
            String etag = webServiceUtil.generateETag(data);
            Response etagMatchResponse = webServiceUtil.getETagMatchResponse(request, etag);
            if (etagMatchResponse != null) {
                return etagMatchResponse;
            }
        }
        else {
            throw new ErrorResponseException(webServiceUtil.getResponseType(),
                                             new ErrorResponse(status.getDetailCode(),
                                                               status.name()),
                                             status.getHttpCode());
        }


        return webServiceUtil.responseWithContent(data);
    }

    @GET
    @Path("externalid/{id}")
    @Produces({"application/xml; charset=utf-8", "application/json; charset=utf-8"})
    public Response getContentWithExternalId(@PathParam("id") String id)
        throws ErrorResponseException
    {
        ContentVersionId latestVersion = webServiceUtil.getContentManager().resolve(id,
                                                                                    Subject.NOBODY_CALLER);
        return webServiceUtil.forwardTo(latestVersion);
    }

    /**
     * We have no permissions check here, because content will only be available if
     * it is public (enforced by the CM client) and has a variant mapping
     * (enforced by WebServiceUtil), which is good enough for most use cases. It is present here
     * mostly to demonstrate that you could add explicit permission checking if you do need it,
     * and also to make it clear that we do not do ACL permission checking by default,
     * because it is very expensive and not suitable for stuff available to the public.
     */
    private boolean checkPermission(ContentVersionId contentId)
        throws ErrorResponseException
    {
        // Insert the following line and remove the aspect section of getContentWithContentId
        // in order to disable access to couchbase content.
        // return IdUtil.isPolicyContentId(contentId.getContentId());
        return true;
    }
}

