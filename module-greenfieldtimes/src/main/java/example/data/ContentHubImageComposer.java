package example.data;

import com.atex.onecms.content.ContentResult;
import com.atex.onecms.content.mapping.ContentComposer;
import com.atex.onecms.content.mapping.Context;
import com.atex.onecms.content.mapping.Request;
import example.greenfieldtimes.adapter.ImageBean;
import example.greenfieldtimes.adapter.ImageResourceBean;

/**
 * Data-api mapper between the new {@link example.greenfieldtimes.adapter.ImageResourceBean}
 * and the old {@link example.greenfieldtimes.adapter.ImageBean}
 */
public class ContentHubImageComposer
    implements ContentComposer<ImageResourceBean, ImageBean, Object>
{
    @Override
    public ContentResult<ImageBean> compose(ContentResult<ImageResourceBean> imageBeanDataResult,
                                            String variant,
                                            Request request,
                                            Context<Object> context)
    {
        ImageResourceBean original = imageBeanDataResult.getContent().getContentData();

        ImageBean imageBean = new ImageBean();
        imageBean.setName(original.getName());
        imageBean.setDescription(original.getDescription());
        imageBean.setImage(original.getFileReference());
        imageBean.setByline(original.getByline());
        imageBean.setContact(original.getContact());
        imageBean.setLicenseUrl(original.getLicenseUrl());

        return new ContentResult<>(imageBeanDataResult, imageBean);
    }
}
