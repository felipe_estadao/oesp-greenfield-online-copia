package example.filter;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.polopoly.cm.ContentId;
import com.polopoly.cm.ExternalContentId;
import com.polopoly.cm.client.CMException;
import com.polopoly.cm.client.ContentRead;
import com.polopoly.cm.path.ContentPathTranslator;
import com.polopoly.cm.policy.PolicyCMServer;
import com.polopoly.cm.servlet.RequestPreparator;

/**
 * Servlet filter that only allows access to derivatives of image files for image content.
 *
 * <p>
 *   The list of input templates for which the derivative validation should be enforced
 *   is configured using the filter initialization parameter 'inputTemplates' as a
 *   comma-separated list of external content IDs.
 * </p>
 *
 * <p>
 *   Note: This filter require a front / preview application setup with a standard path
 *   translator and CM application.
 * </p>
 */
public class OriginalImageFilter
    implements Filter
{
    private static final String INPUT_TEMPLATES_INIT_PARAM_NAME = "inputTemplates";
    private static final Logger LOG = Logger.getLogger(OriginalImageFilter.class.getName());

    private Set<String> imageTemplateExternalIds = new HashSet<String>();

    private String filterName = null;
    private Pattern derivitivePattern = null;

    @Override
    public void init(final FilterConfig filterConfig)
        throws ServletException
    {
        filterName = filterConfig.getFilterName();

        String templates = filterConfig.getInitParameter(INPUT_TEMPLATES_INIT_PARAM_NAME);

        if (templates == null) {
            LOG.config(filterName + ": Initialized with empty input templates list. No derivative filtering will be performed!");

            return;
        }

        for (String template : templates.split("[,\n]")) {
            imageTemplateExternalIds.add(template.trim());
        }

        derivitivePattern = Pattern.compile(".*_gen/derivatives/.*");

        LOG.config(String.format("%s: Initialized with input templates list '%s'.", filterName, imageTemplateExternalIds));
        LOG.config(String.format("%s: Using derivitive pattern '%s'.", filterName, derivitivePattern));
    }

    @Override
    public void doFilter(final ServletRequest request,
                         final ServletResponse response,
                         final FilterChain filterChain)
        throws IOException, ServletException
    {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;

        ContentPathTranslator pathTranslator = RequestPreparator.getFilePathTranslator(httpServletRequest);
        PolicyCMServer cmServer = RequestPreparator.getCMServer(httpServletRequest);

        /**
         * In all cases where we don't have enough information to decide for
         * ourselves whether to let a request through or not, we let the file
         * servlet decide instead.
         */

        if (pathTranslator == null || cmServer == null) {
            filterChain.doFilter(request, response);

            return;
        }

        String pathInfo = httpServletRequest.getPathInfo();
        int pathIndex = pathInfo.lastIndexOf('!');

        if (pathIndex == -1) {
            filterChain.doFilter(request, response);

            return;
        }

        String pathToContent = pathInfo.substring(0, pathIndex);
        String pathInContent = pathInfo.substring(pathIndex + 1);

        try {
            ContentId contentId = pathTranslator.getContentId(pathToContent);
            ContentRead content = cmServer.getContent(contentId);

            ContentId inputTemplateId = content.getInputTemplateId();
            ExternalContentId inputTemplateExternalId = cmServer.getContent(inputTemplateId).getExternalId();

            if (!imageTemplateExternalIds.contains(inputTemplateExternalId.getExternalId())) {
                filterChain.doFilter(request, response);

                return;
            }

            if (derivitivePattern.matcher(pathInContent).matches()) {
                filterChain.doFilter(request, response);
            } else {
                ((HttpServletResponse) response).sendError(HttpServletResponse.SC_NOT_FOUND);
            }
        } catch (CMException e) {
            filterChain.doFilter(request, response);
        }
    }

    @Override
    public void destroy()
    {

    }
}
