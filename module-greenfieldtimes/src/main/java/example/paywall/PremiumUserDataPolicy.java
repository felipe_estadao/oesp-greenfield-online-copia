package example.paywall;

import com.polopoly.cm.ContentId;
import com.polopoly.cm.client.CMException;
import com.polopoly.cm.policy.ContentPolicy;
import com.polopoly.paywall.Capability;
import com.polopoly.paywall.ContentBundle;
import com.polopoly.paywall.Offering;
import com.polopoly.paywall.Subscription;
import com.polopoly.paywall.SubscriptionFieldPolicy;

import java.util.Collection;
import java.util.HashSet;

/**
 * Policy used to store paywall related data on site users.
 * Uses the <code>p.Subscription</code> field.
 */
public class PremiumUserDataPolicy extends ContentPolicy {

    public void buyOffering(Offering offering) throws CMException {
        getSubscriptionFieldPolicy().buy(offering);
    }

    public Collection<ContentId> getAccessibleContentBundlesByCapability(Capability capability) throws CMException {
        Collection<ContentId> accessibleBundleIds = new HashSet<ContentId>();
        Collection<ContentBundle> bundles = getSubscriptionFieldPolicy().getContentBundlesByCapability(capability);
        for (ContentBundle bundle : bundles) {
            accessibleBundleIds.add(bundle.getContentId());
        }
        return accessibleBundleIds;
    }

    public Collection<Subscription> getSubscriptions() throws CMException {
        return getSubscriptionFieldPolicy().getSubscriptions();
    }

    public void clearSubscriptions() throws CMException {
        getSubscriptionFieldPolicy().clearSubscriptions();
    }

    private SubscriptionFieldPolicy getSubscriptionFieldPolicy() throws CMException {
        return (SubscriptionFieldPolicy) getChildPolicy("subscription");
    }
}
