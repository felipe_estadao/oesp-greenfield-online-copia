package example.layout.element.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.polopoly.cm.ExternalContentId;
import com.polopoly.cm.app.search.categorization.TypeProvider;
import com.polopoly.cm.client.CMException;

import example.layout.element.ElementPolicy;

public class SearchElementPolicy
    extends ElementPolicy
{
    private static final String INPUT_TEMPLATES_SELECT_CHILD = "inputTemplates";
    private static final Logger LOG = Logger.getLogger(SearchElementPolicy.class.getName());

    /**
     * Returns the external content IDs of input templates that search queries should be filtered on.
     * @return an array of configured input template ExternalContentIds as Strings.
     */
    protected String[] getInputTemplatesForFilter()
    {
        Set<ExternalContentId> types = Collections.emptySet();

        try {
            TypeProvider typeProvider = (TypeProvider) getChildPolicy(INPUT_TEMPLATES_SELECT_CHILD);

            if (typeProvider != null) {
                types = typeProvider.getTypes();
            }
        } catch (CMException cme) {
            LOG.log(Level.WARNING, "Unable to read configured input templates from field!", cme);
        }

        ArrayList<String> typesList = new ArrayList<String>();

        for (ExternalContentId externalId : types) {
            typesList.add(externalId.getExternalId());
        }

        return typesList.toArray(new String[types.size()]);
    }
}
