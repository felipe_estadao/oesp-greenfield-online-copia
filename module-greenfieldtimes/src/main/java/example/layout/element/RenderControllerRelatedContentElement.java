package example.layout.element;

import com.polopoly.cm.ContentId;
import com.polopoly.cm.client.CMException;
import com.polopoly.cm.policy.Policy;
import com.polopoly.cm.policy.PolicyCMServer;
import com.polopoly.metadata.Metadata;
import com.polopoly.metadata.MetadataAware;
import com.polopoly.model.Model;
import com.polopoly.model.ModelPathUtil;
import com.polopoly.render.CacheInfo;
import com.polopoly.render.RenderRequest;
import com.polopoly.siteengine.dispatcher.ControllerContext;
import com.polopoly.siteengine.model.TopModel;

import example.content.RenderControllerExtended;

public class RenderControllerRelatedContentElement extends RenderControllerExtended {

    @Override
    public void populateModelAfterCacheKey(RenderRequest request,
                                           TopModel m,
                                           CacheInfo cacheInfo,
                                           ControllerContext context)
    {
        super.populateModelAfterCacheKey(request, m, cacheInfo, context);

        Metadata metadata = new Metadata();
        Model contentModel = context.getContentModel();
        try {
            Policy policy = (Policy) ModelPathUtil.getBean(contentModel);
            PolicyCMServer cmServer = policy.getCMServer();

            while (policy != null && !(policy instanceof MetadataAware)) {
                ContentId securityParentId = policy.getContent().getSecurityParentId();
                if (securityParentId != null) {
                    policy = cmServer.getPolicy(securityParentId);
                }
                else {
                    policy = null;
                    break;
                }
            }

            if (policy instanceof MetadataAware) {
                metadata = ((MetadataAware) policy).getMetadata();
            }
        }
        catch (CMException e) {
            // We can't get categorization, so we just set an empty categorization
        }
        m.getLocal().setAttribute("metadata", metadata);
    }
}
