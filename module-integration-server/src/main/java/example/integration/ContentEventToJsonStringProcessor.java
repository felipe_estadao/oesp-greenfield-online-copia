package example.integration;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.polopoly.cm.event.ContentEvent;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Processor that replaces the message body content event with a string
 * representation
 */
public class ContentEventToJsonStringProcessor implements Processor {
    private Gson gson = new Gson();

    public void process(Exchange exchange) throws Exception {

        Object body = exchange.getIn().getBody();
        if (body instanceof ContentEvent) {
            ContentEvent contentEvent = (ContentEvent) body;
            JsonObject jsonMap = new JsonObject();
            jsonMap.addProperty("principalIdString", contentEvent.principalIdString);
            jsonMap.addProperty("eventType", contentEvent.eventType);
            jsonMap.addProperty("contentId", contentEvent.getContentId().getContentIdString());
            jsonMap.addProperty("timestamp", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
            exchange.getIn().setBody(gson.toJson(jsonMap) + "\n");
        } else {
            exchange.setProperty(Exchange.ROUTE_STOP, Boolean.TRUE);
        }
    }
}
